# Jitsi instance setup using Ansible and Docker

## Purpose

This Ansible playbook is meant to easily let you run your own [Jitsi](https://jitsi.org/) instance.
